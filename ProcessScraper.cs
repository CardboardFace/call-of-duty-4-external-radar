﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;

namespace COD4_External_Radar
{
    [Obsolete("Use GuidedHackingExtension instead.")]
    class ProcessScraper
    {
        protected int handle;
        private readonly string processName;

        // Constructor
        public ProcessScraper(string processName)
        {
            this.processName = processName;
        }

        /// <summary>
        /// Checks if a Window handle is still valid
        /// </summary>
        /// <param name="hWnd">True if valid</param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool IsWindow(int hWnd);


        /// <summary>
        ///     Returns a handle for functions such as the wait functions, provided the appropriate access rights were requested.
        ///     When finished with the handle, be sure to close it using CloseHandle.
        /// </summary>
        /// <param name="dwDesitedAccess">
        ///     The access to the process object. This access right is checked against the security descriptor for the process. This parameter can be one or more of the process access rights.
        ///     If the caller has enabled the SeDebugPrivilege privilege, the requested access is granted regardless of the contents of the security descriptor.
        /// </param>
        /// <param name="bInheritHandle">If this value is TRUE, processes created by this process will inherit the handle. Otherwise, the processes do not inherit this handle.</param>
        /// <param name="dwProcessId">The identifier of the local process to be opened.</param>
        /// <returns>
        ///     If the function succeeds, the return value is an open handle to the specified process.
        ///     If the function fails, the return value is NULL.To get extended error information, call GetLastError.
        /// </returns>
        [DllImport("Kernel32.dll", SetLastError = true)]
        private static extern int OpenProcess(ProcessAccessFlags dwDesitedAccess, bool bInheritHandle, int dwProcessID);


        /// <summary>
        ///     Process-specific access right flags.
        ///     More info: https://docs.microsoft.com/en-us/windows/win32/procthread/process-security-and-access-rights
        /// </summary>
        [Flags]
        enum ProcessAccessFlags : uint
        {
            All = 0x001F0FFF, // More likely to be detected by anti-cheats
            Terminate = 0x00000001,
            CreateThread = 0x00000002,
            VMOperation = 0x00000008, // Required to perform operation on an address space (VirtualProtectEx, WriteProcessMemory)
            VMRead = 0x00000010, // Required for ReadProcessMemory
            VMWrite = 0x00000020, // Required for WriteProcessMemory
            DupHandle = 0x00000040,
            SetInformation = 0x00000200,
            QueryInformation = 0x00000400,
            Synchronize = 0x00100000, // Required for wait functions
            ReadControl = 0x00020000
        }


        /// <summary>
        ///     Closes handles to the following objects: https://docs.microsoft.com/en-us/windows/win32/api/handleapi/nf-handleapi-closehandle#remarks
        /// </summary>
        /// <param name="hObject">A valid handle to an open object.</param>
        /// <returns>
        ///     If the function succeeds, the return value is nonzero.
        ///     If the function fails, the return value is zero.To get extended error information, call GetLastError.
        /// </returns>
        [DllImport("Kernel32.dll", SetLastError = true)]
        private static extern bool CloseHandle(int hObject);

        /// <summary>
        ///     Copies data in the specified address range from the address space of the specified process into the specified buffer of the current process. Any process that has a handle with PROCESS_VM_READ access can call the function.
        ///     The entire area to be read must be accessible, and if it is not accessible, the function fails.
        /// </summary>
        /// <param name="hProcess">A handle to the process with memory that is being read. The handle must have PROCESS_VM_READ access to the process.</param>
        /// <param name="lpBaseAddress">A pointer to the base address in the specified process from which to read. Before any data transfer occurs, the system verifies that all data in the base address and memory of the specified size is accessible for read access, and if it is not accessible the function fails.</param>
        /// <param name="lpBuffer">A pointer to a buffer that receives the contents from the address space of the specified process.</param>
        /// <param name="nSize">The number of bytes to be read from the specified process.</param>
        /// <param name="lpNumberOfBytesRead">A pointer to a variable that receives the number of bytes transferred into the specified buffer. If lpNumberOfBytesRead is NULL, the parameter is ignored.</param>
        /// <returns>
        ///     If the function succeeds, the return value is nonzero. 
        ///     If the function fails, the return value is 0 (zero). To get extended error information, call GetLastError.
        ///     The function fails if the requested read operation crosses into an area of the process that is inaccessible.
        /// </returns>
        [DllImport("kernel32.dll")]
        static extern bool ReadProcessMemory(int hProcess, int lpBaseAddress, byte[] lpBuffer, int dwSize, ref int lpNumberOfBytesRead);

        //static extern bool ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, [Out] byte[] lpBuffer, int dwSize, out IntPtr lpNumberOfBytesRead);
        //[DllImport("Kernel32.dll", SetLastError = true)]
        //private static extern bool ReadProcessMemory(int hProcess, int lpBase, ref byte[] lpBuffer, int nSize, int lpNumberOfBytesRead);

        /// <summary>
        ///     Helper function to load byte arrays into structures
        /// </summary>
        /// <typeparam name="T">The structure type</typeparam>
        /// <param name="bytes">The bytes to read into an instance</param>
        /// <returns>A filled instance of the structure</returns>
        public static T ByteArrayToStructure<T>(byte[] bytes) where T : struct
        {
            GCHandle handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
            T stuff = (T)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(T));
            handle.Free();
            return stuff;
        }

        /// <summary>
        /// Locates a process.
        /// </summary>
        /// <param name="ProcessName">The name of the process to locate (not the file name)</param>
        /// <returns>The process object</returns>
        private static Process FindProcess(string processName)
        {
            Process proc = (from Process p in Process.GetProcesses()
                            where p.ProcessName.ToLower() == processName.ToLower()
                            select p).FirstOrDefault();
            return proc;
        }

        /// <summary>
        /// Locates a module's base address.
        /// </summary>
        /// <param name="proc">The target process to search within</param>
        /// <param name="ModuleName">The name of a module to look for, for example: "ac_client.exe"</param>
        /// <returns>The base address of the target module or zero if unsucessful</returns>
        public static IntPtr FindModuleBaseAddress(Process process, String moduleName)
        {
            if (process == null)
                return IntPtr.Zero;

            ProcessModule mod = (from ProcessModule m in process.Modules
                                 where m.ModuleName.ToLower() == moduleName.ToLower()
                                 select m).FirstOrDefault();
            return mod.BaseAddress;
        }

        /// <summary>Opens a Windows Handle for the ProcScraper instance target process</summary>
        /// <returns>True if successful</returns>
        public bool OpenProccessHandle()
        {
            Process targetProc = FindProcess(this.processName);
            if (targetProc == null) return false; // Don't open if proc is invalid

            this.handle = OpenProcess(ProcessAccessFlags.VMRead, false, targetProc.Id); // Attempt to open handle
            return this.handle != 0;
        }

        /// <summary>
        /// Closes the handle for the target process
        /// </summary>
        /// <returns>True if successful</returns>
        public bool CloseProcessHandle()
        {
            return CloseHandle(this.handle);
        }

        /// <summary>Overwrites memory from address onwards, depending on size of newData. Unsafe improves performance.</summary>
        /// <param name="address">The target address</param>
        /// <param name="numberOfBytes">The number of bytes to read</param>
        /// <returns>Byte array read from process</returns>
        public unsafe byte[] ReadMemory(int address, int numberOfBytes)
        {
            int bytesRead = 0;
            byte[] buffer = new byte[numberOfBytes];
            if (!ReadProcessMemory(this.handle, address, buffer, buffer.Length, ref bytesRead))
            {
                this.CloseProcessHandle();
                Environment.Exit(1); //throw new ArgumentException("ERROR: Cannot ReadProcessMemory!");
            }

            return buffer;
        }
    }
}