﻿using System.Runtime.InteropServices;
using System.Numerics;
using System;
using System.Linq;


namespace COD4_External_Radar
{
    public enum Stance : short
    {
        Prone = 62,
        Crouched = 82,
        Standing = 102
    };

    public enum Weapon : int
    {
        AK47 = 9,
        AKSU = 10,
        RPG = 11,
        USP = 12,
        Dragunov = 13,
        AK47Grenadier = 14,
        M9 = 16,
        W1200 = 19,
        M4A1 = 22
    };

    public enum AnimalType : uint
    {
        Unloaded = 0,
        USA = 1,
        Spetsnaz = 2,
        SAS = 3,
        Dog = 4,
        USA_Invincible = 5,
    };

    [StructLayout(LayoutKind.Explicit)]
    public struct AnimalInfo
    {
        [FieldOffset(0x00)] public int PtrToAnimal;
        [FieldOffset(0x0C)] public AnimalType Type;

        /// <summary>
        /// Determines if the animal's icon should be allowed to rotate with the compass
        /// </summary>
        /// <returns>True if it shouldn't rotate</returns>
        public bool ShouldDisplayUpright()
        {
            return Type == AnimalType.Dog;
        }

        /// <summary>
        /// Determines if a team has been set for the animal yet
        /// </summary>
        /// <returns>True if a team is assigned</returns>
        public bool IsTeamValid()
        {
            return Type != AnimalType.Unloaded;
        }

        /// <summary>
        /// Determines if the animal is on the player's team
        /// </summary>
        /// <returns>True if friendly</returns>
        public bool IsFriendly()
        {
            AnimalType[] friendlies = { AnimalType.USA, AnimalType.SAS, AnimalType.USA_Invincible };
            return friendlies.Contains(Type);
        }
    }

    [StructLayout(LayoutKind.Explicit)]
    public struct Animal
    {
        [FieldOffset(0x02)] public Weapon WeaponID;
        [FieldOffset(0x10)] public uint TimeAlive;
        [FieldOffset(0x18)] public Vector3 Position;
        [FieldOffset(0x24)] public Vector3 Velocity;
        [FieldOffset(0x3C)] public Vector3 Angles;
        [FieldOffset(0x86)] public Stance Stance;
        [FieldOffset(0x88)] public uint AnimCount;
        [FieldOffset(0xC8)] public Vector3 TorsoPosition;
        [FieldOffset(0xD4)] public Vector3 HeadPosition;
        [FieldOffset(0xE0)] public Vector3 PositionCopy;
        [FieldOffset(0xF0)] public float AimYaw;
        [FieldOffset(0x144)] public uint Health;
        [FieldOffset(0x148)] public uint MaxHealth;

        public bool IsAlive()
        {
            return !(Health < 1 || Health > 4294960000); // Health often jumps above 4,294,960,000 when a bot dies
        }
    }

    /// <summary>
    ///		Used to create an array of Animal structures, reading from the game's process
    /// </summary>
    class AnimalList 
    {
        public static readonly int MaximumAnimals = 32;
        private static readonly int AnimalListAddr = 0xDCF7A0; // Address of the list of animals
        private static readonly int AnimalPtrPadding = 0x74; // Distance in memory between pointers to animal class instances in the animal list array
        private static readonly int BytesInAnimalStruct = 1024; // Maximum size of Animal structure in bytes (can be above)

        private readonly bool[] animalValidityStatuses = new bool[MaximumAnimals];
        public AnimalInfo[] animalInfoList;
        public Animal[] animalList;
        private readonly ProcessScraper proc;

        /// <summary>
        ///		Constructs an array of animal objects, including the player (at index 0) and any AI.
        /// </summary>
        /// <param name="proc">The COD4 process</param>
        public AnimalList(ProcessScraper proc)
        {
            this.proc = proc;
            this.animalList = new Animal[MaximumAnimals];
            this.animalInfoList = new AnimalInfo[MaximumAnimals];
        }

        /// <summary>
        ///		Reloads the cached data about an animal from memory, by their index
        /// </summary>
        /// <param name="animalIndex">The animal's position in the animal/entity list</param>
        /// <returns>True if animal is valid</returns>
        private bool UpdateAnimal(AnimalInfo animalInfo, int animalIndex)
        {
            // Check if animal base address is valid
            if (animalInfo.PtrToAnimal == 0) return false;

            byte[] rawStruct = proc.ReadMemory(animalInfo.PtrToAnimal, BytesInAnimalStruct); // Read animal instance
            animalList[animalIndex] = ProcessScraper.ByteArrayToStructure<Animal>(rawStruct); // Convert animal instance binary into structure

            // Check if every byte in the animal is zero
            if (rawStruct.All(singleByte => singleByte == 0)) return false;

            return true;
        }

        /// <summary>
        ///		Recaches all players to AnimalList structures for UI
        /// </summary>
        /// <returns>How many valid animals are in-game</returns>
        public void UpdateAnimals()
        {
            // Read all AnimalInfos from process
            byte[] rawAnimalInfos = proc.ReadMemory(AnimalListAddr, MaximumAnimals * AnimalPtrPadding);

            int animalIndex;
            for (animalIndex = 0; animalIndex < MaximumAnimals; animalIndex++)
            {
                // Read AnimalInfo into byte array
                byte[] rawAnimalInfo = new byte[AnimalPtrPadding];
                Buffer.BlockCopy(rawAnimalInfos, animalIndex * AnimalPtrPadding, rawAnimalInfo, 0, AnimalPtrPadding);

                // Convert animal info binary into structure
                AnimalInfo animalInfo = ProcessScraper.ByteArrayToStructure<AnimalInfo>(rawAnimalInfo);
                animalInfoList[animalIndex] = animalInfo;

                animalValidityStatuses[animalIndex] = UpdateAnimal(animalInfo, animalIndex);
            }
        }

        /// <summary>
        /// Returns true if a human is valid/spawned
        /// </summary>
        /// <param name="humanIndex">The index of the human in the game's humanList</param>
        /// <returns>True if valid</returns>
        public bool IsAnimalValid(int humanIndex)
        {
            return animalValidityStatuses[humanIndex];
        }
    }
}