﻿using System;

namespace COD4_External_Radar.Exceptions
{
    class MemoryAccessException : Exception
    {
        public MemoryAccessException(string message)
        : base(message)
        {
        }

        public MemoryAccessException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
