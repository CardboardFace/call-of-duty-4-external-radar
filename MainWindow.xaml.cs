﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace COD4_External_Radar
{
    public class AnimalIcon
    {
        public Path Path;
        public AnimalType Type;
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Settings
        private readonly static int mapPadding = -10; // Helps to prevent pop-in
        private static float mapScale = 0.03F; // How far away to draw animals from the center of the compass
        private readonly static float mapMaxZoom = 0.09F; // Maximum you can zoom in
        private readonly static float mapMinZoom = 0.008F;

        private ProcessScraper cod;
        private AnimalIcon[] animalIcons; // Array to hold all arrows/icons for AI animals
        private int playerIndex; // Holds the player's animal index

        public MainWindow()
        {
            InitializeComponent();

            this.animalIcons = new AnimalIcon[AnimalList.MaximumAnimals];

            cod = new ProcessScraper("iw3sp");
            while (!cod.OpenProccessHandle())
            {
                MessageBoxResult result = MessageBox.Show("iw3sp.exe process is not running. Retry?", "OpenHandle error", MessageBoxButton.OKCancel, MessageBoxImage.Error);
                if (result == MessageBoxResult.Cancel) Environment.Exit(1);
            }

            // Warn user if their performance settings are limiting
            if (RenderCapability.Tier / 0x10000 != 2) DisplayWarning("Hardware acceleration is off!");
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Setup AnimalList instance
            AnimalList animalsInGame = new(cod);

            // Setup transform for compass & icons rotation
            MainCanvas.RenderTransform = new RotateTransform();
            MainCanvas.RenderTransformOrigin = new Point(0.5, 0.5);

            // Endlessly run updates
            while (true)
            {
                // Update data in the background (asynchrinously, to avoid UI hangs)
                await Task.Run(() => animalsInGame.UpdateAnimals());

                // Update UI
                UpdateUI(animalsInGame);

                // Small sleep to avoid high resource usuage
                await Task.Delay(8); // Run around 120fps
            }
        }

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            // Close COD process handle
            cod.CloseProcessHandle();
        }

        /// <summary>
        /// Double click to maximise. Double right click to close.
        /// </summary>
        private void Window_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Right)
            {
                Environment.Exit(1);
            }
            if (this.WindowState == WindowState.Normal)
            {
                this.WindowState = WindowState.Maximized;
            } else
            {
                this.WindowState = WindowState.Normal;
            }
        }

        /// <summary>
        /// Click-drag anywhere to move window
        /// </summary>
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left) this.DragMove();
        }

        private void Window_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            int direction = Math.Clamp(e.Delta, -1, 1);

            mapScale += direction * 0.003F;
            mapScale = Math.Clamp(mapScale, mapMinZoom, mapMaxZoom);
        }

        /// <summary>
        /// Displays a warning string in case of error
        /// </summary>
        /// <param name="warning">The warning to display</param>
        private void DisplayWarning(string warning)
        {
            Warnings.Content = "WARNING: " + warning;
            Warnings.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Sets the health UI
        /// </summary>
        /// <param name="health">The player's health</param>
        private void UpdateHealthUI(uint health)
        {
            LinearGradientBrush healthBar = (LinearGradientBrush)Stance_Icon.Fill;
            foreach (GradientStop gradientStop in healthBar.GradientStops)
            {
                gradientStop.Offset = 1 - Convert.ToDouble(health) / 100;
            }
        }

        /// <summary>
        /// Updates the stance icon representing the player's stance
        /// </summary>
        /// <param name="stance">The uint16 representing the player's stance</param>
        private void UpdateStanceUI(Stance stance)
        {
            Path newIcon = stance switch
            {
                Stance.Crouched => (Path)Application.Current.Resources["Stance_Crouched"],
                Stance.Prone => (Path)Application.Current.Resources["Stance_Prone"],
                _ => (Path)Application.Current.Resources["Stance_Standing"],
            };
            Stance_Icon.Data = newIcon.Data;
            Stance_Icon.Height = newIcon.Height;
            Stance_Icon.Width = newIcon.Width;
            Canvas.SetTop(Stance_Icon, Canvas.GetTop(newIcon));
        }

        /// <summary>
        /// Spins the minimap
        /// </summary>
        /// <param name="yaw">The angle to rotate to</param>
        private void RotateMap(float yaw)
        {
            ((RotateTransform)MainCanvas.RenderTransform).Angle = yaw;
        }

        /// <summary>
        /// Duplicates app resources so that they can be reused multiple times on the same canvas
        /// </summary>
        /// <param name="resourceName">The app resource key</param>
        /// <returns>The Object from the Application Resources</returns>
        private static object GetAppResourceCopy(string resourceName)
        {
            object targetResource = Application.Current.Resources[resourceName];
            string resourceXaml = XamlWriter.Save(targetResource);

            System.IO.StringReader stringReader = new System.IO.StringReader(resourceXaml);
            XmlReader xmlReader = XmlReader.Create(stringReader);

            return XamlReader.Load(xmlReader);
        }

        /// <summary>
        /// Spins a animal Path object to the desired angle
        /// </summary>
        /// <param name="animalIcon">The icon to spin</param>
        /// <param name="yaw">The angle to rotate to</param>
        private static void RotateAnimalIcon(AnimalIcon animalIcon, float yaw)
        {
            ((RotateTransform)((TransformGroup)animalIcon.Path.RenderTransform).Children[0]).Angle = yaw;
        }

        /// <summary>
        /// Resizes a animal icon
        /// </summary>
        /// <param name="animalIcon">The icon to resize</param>
        /// <param name="scale">The desired size</param>
        private static void ScaleAnimalIcon(AnimalIcon animalIcon, double scale)
        {
            ScaleTransform iconTrasform = ((ScaleTransform)((TransformGroup)animalIcon.Path.RenderTransform).Children[1]);
            if (iconTrasform.ScaleX == scale) return;
            iconTrasform.ScaleX = scale;
            iconTrasform.ScaleY = scale;
        }

        /// <summary>
        /// Gets the appropriate path for an animal
        /// </summary>
        /// <param name="animal">The animal, used to check its health</param>
        /// <param name="animalInfo">The animalInfo to check the team/ent type</param>
        /// <returns></returns>
        private static Path GetAnimalPathData(Animal animal, AnimalInfo animalInfo)
        {
            if (animalInfo.IsFriendly()) // Check animal is friendly
            {
                if (animal.Health > 10000) // Animals like Cpt. Price or Lt. Vaquez have 100,000,000 HP
                {
                    return (Path)GetAppResourceCopy("Captain_Icon");
                }
                else
                {
                    return (Path)GetAppResourceCopy("Friendly_Icon");
                }
            }
            else if (animalInfo.Type == AnimalType.Dog)
            {
                return (Path)GetAppResourceCopy("Dog_Icon");
            }
            else
            {
                return (Path)GetAppResourceCopy("Enemy_Icon");
            }
        }

        /// <summary>
        /// Creates an icon for a animal with a team-appropritate colour
        /// </summary>
        /// <param name="animal"></param>
        /// <param name="animalInfo"></param>
        /// <returns>A animal icon</returns>
        private AnimalIcon BuildAnimalIcon(Animal animal, AnimalInfo animalInfo)
        {
            AnimalIcon animalIcon = new()
            {
                Path = GetAnimalPathData(animal, animalInfo)
            };

            // Setup transform for icon rotation
            TransformGroup animalIconTransformer = new();
            animalIconTransformer.Children.Add(new RotateTransform());
            animalIconTransformer.Children.Add(new ScaleTransform());
            animalIcon.Path.RenderTransform = animalIconTransformer;
            animalIcon.Path.RenderTransformOrigin = new Point(0.5, 0.5);

            // Add icon to canvas
            MainCanvas.Children.Add(animalIcon.Path);

            // Save what type the path was drawn as to the icon
            animalIcon.Type = animalInfo.Type;

            return animalIcon;
        }

        /// <summary>
        /// Removes an animal icon from the canvas
        /// </summary>
        /// <param name="animalIcon">The icon to remove</param>
        private void RemoveAnimalIcon(ref AnimalIcon animalIcon)
        {
            if (animalIcon == null) return; // Save performance by not removing null icons
            MainCanvas.Children.Remove(animalIcon.Path);
            animalIcon = null;
        }

        /// <summary>
        /// Gets the player's position relative to the UI
        /// </summary>
        /// <returns>Center of the compass</returns>
        private Vector3 GetScaledPlayerPos()
        {
            return new Vector3(Convert.ToSingle(MainCanvas.ActualWidth / 2), Convert.ToSingle(MainCanvas.ActualHeight / 2), 0F);
        }

        /// <summary>
        /// Gets the position of an animal, relative to the player and scaled for rendering on the WPF form
        /// </summary>
        /// <param name="player">The player animal instance</param>
        /// <param name="animal">The target animal</param>
        /// <returns>Relative position</returns>
        private Vector3 GetRelativeScaledPosition(Animal player, Animal animal)
        {
            Vector3 relativePos = Vector3.Subtract(player.Position, animal.Position);
            float left = GetScaledPlayerPos().X + relativePos.Y * mapScale;
            float top = GetScaledPlayerPos().Y + relativePos.X * mapScale;
            return new Vector3(left, top, 0);
        }

        /// <summary>
        /// Redraws the UI that represents an AI/animal.
        /// </summary>
        /// <param name="player">The player (also animal) to draw this animal relative to</param>
        /// <param name="animal">The animal to draw</param>
        /// <param name="animalInfo">The animal's accompanying animalInfo object (contains team)</param>
        /// <param name="animalIcon">The animalIcon to use. It'll create an icon if none exists or remove it if the animal is invalid</param>
        private void DrawAnimal(Animal player, Animal animal, AnimalInfo animalInfo, ref AnimalIcon animalIcon)
        {
            Vector3 relativePos = GetRelativeScaledPosition(player, animal);

            // Check if animal is dead or too far away
            // DistanceSquared is more efficient than Distance
            double CompassRadius = CompassInside.Width / 2;
            if (!animal.IsAlive() || !animalInfo.IsTeamValid() || Vector3.DistanceSquared(GetScaledPlayerPos(), relativePos) > Math.Pow(CompassRadius - mapPadding, 2))
            {
                RemoveAnimalIcon(ref animalIcon); // Cleanup icon
                return;
            }

            // Make an icon if one doesn't exist
            if (animalIcon == null) // || !MainCanvas.Children.Contains(animalIcon.Path))
            {
                animalIcon = BuildAnimalIcon(animal, animalInfo);
            }

            // Check if the animal's type changed (can happen as an ent loads in)
            if (animalInfo.Type != animalIcon.Type)
            {
                animalIcon.Type = animalInfo.Type; // Update flag
                animalIcon.Path = GetAnimalPathData(animal, animalInfo); // Update icon
            }

            // Rotate icon
            if (animalInfo.ShouldDisplayUpright())
            {
                RotateAnimalIcon(animalIcon, -player.Angles.Y); // Counteract the minimap rotation
            } else
            {
                RotateAnimalIcon(animalIcon, -animal.Angles.Y);
            }

            // Resize icon if enemy is above or below player
            float heightAboveAnimal = player.Position.Z - animal.Position.Z; // How much higher you are than the animal (Z difference)
            if (heightAboveAnimal > 100)
            {
                ScaleAnimalIcon(animalIcon, 0.5);
            } else if (heightAboveAnimal < -100)
            {
                ScaleAnimalIcon(animalIcon, 0.75);
            } else
            {
                ScaleAnimalIcon(animalIcon, 0.62);
            }

            // Update position
            Canvas.SetLeft(animalIcon.Path, relativePos.X - (animalIcon.Path.ActualWidth / 2));
            Canvas.SetTop(animalIcon.Path, relativePos.Y - (animalIcon.Path.ActualHeight / 2));
        }

        /// <summary>
        /// Locates which animal is the player within an array of animals.
        /// </summary>
        /// <param name="animalsInGame">The array of animals currently in the level.</param>
        /// <returns>An index integer representing which position in the animalList is the player</returns>
        private static int FindPlayerIndex(Animal[] animalsInGame)
        {
            for(int i = 0; i < animalsInGame.Length; i++)
            {
                if (animalsInGame[i].TimeAlive > 0) return i;
            }
            return 0;
        }

        /// <summary>
        /// Redraws the UI from the current data stored on the animals in-game
        /// </summary>
        /// <param name="animalsCount">The number of valid animals to update for</param>
        /// <param name="animalsInGame">A list of animal objects in the game</param>
        private void UpdateUI(AnimalList animalsInGame)
        {
            // Get player animal
            // Sometimes the animalList is reordered and the player isn't first (i.e. "The Bog" map)
            // Only the player uses TimeAlive, so hunt for that
            // This is checked each update in case the list reorders (level change)
            Animal player = animalsInGame.animalList[playerIndex];
            if (player.TimeAlive == 0) {
                playerIndex = FindPlayerIndex(animalsInGame.animalList);
                player = animalsInGame.animalList[playerIndex];
            };

            UpdateHealthUI(player.Health);
            UpdateStanceUI(player.Stance);
            RotateMap(player.Angles.Y);

            // Draw enemies
            for (int i = 0; i < AnimalList.MaximumAnimals; i++)
            {
                if (i == playerIndex) continue; // Skip player

                if (animalsInGame.IsAnimalValid(i))
                {
                    DrawAnimal(player, animalsInGame.animalList[i], animalsInGame.animalInfoList[i], ref animalIcons[i]);
                } else
                {
                    RemoveAnimalIcon(ref animalIcons[i]); // Cleanup icon
                }
            }
        }
    }
}
