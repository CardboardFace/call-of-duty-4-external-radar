﻿using COD4_External_Radar.Exceptions;
using GuidedHackingLib;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace COD4_External_Radar
{
    public class GuidedHackingExtension : GuidedHacking {
        protected IntPtr handle;
        protected Process process;

        // Constructor
        public GuidedHackingExtension(Process process)
        {
            this.process = process;
        }

        /// <summary>
        /// Checks if a Window handle is still valid
        /// </summary>
        /// <param name="hWnd">True if valid</param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool IsWindow(int hWnd);


        [DllImport("kernel32.dll")]
        static extern uint GetLastError();

        //static extern bool ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, [Out] byte[] lpBuffer, int dwSize, out IntPtr lpNumberOfBytesRead);
        //[DllImport("Kernel32.dll", SetLastError = true)]
        //private static extern bool ReadProcessMemory(int hProcess, int lpBase, ref byte[] lpBuffer, int nSize, int lpNumberOfBytesRead);

        /// <summary>
        ///     Helper function to load byte arrays into structures
        /// </summary>
        /// <typeparam name="T">The structure type</typeparam>
        /// <param name="bytes">The bytes to read into an instance</param>
        /// <returns>A filled instance of the structure</returns>
        public static T ByteArrayToStructure<T>(byte[] bytes) where T : struct {
            GCHandle handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
#pragma warning disable CS8605 // Unboxing a possibly null value.
            T stuff = (T)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(T));
#pragma warning restore CS8605 // Unboxing a possibly null value.
            handle.Free();
            return stuff;
        }

        /// <summary>
        /// Locates a process.
        /// </summary>
        /// <param name="ProcessName">The name of the process to locate (not the file name)</param>
        /// <returns>The process object</returns>
        public static Process FindProcess(string processName) {
            Process[] processes = Process.GetProcessesByName(processName);
            if (processes.Length == 0) throw new AppNotRunningException();

            return processes[0];
        }

        /// <summary>Opens a Windows Handle for the ProcScraper instance target process</summary>
        /// <returns>True if successful</returns>
        public bool OpenProccessHandle() {
            this.handle = OpenProcess(ProcessAccessFlags.All, false, this.process.Id); // Attempt to open handle
            return this.handle != IntPtr.Zero;
        }

        /// <summary>
        /// Closes the handle for the target process
        /// </summary>
        /// <returns>True if successful</returns>
        public bool CloseProcessHandle() {
            return CloseHandle(this.handle);
        }

        /// <summary>Overwrites memory from address onwards, depending on size of newData. Unsafe improves performance.</summary>
        /// <param name="address">The target address</param>
        /// <param name="numberOfBytes">The number of bytes to read</param>
        /// <returns>Byte array read from process</returns>
        public unsafe byte[] ReadMemory(IntPtr address, int numberOfBytes) {
            byte[] buffer = new byte[numberOfBytes];
            if (!ReadProcessMemory(this.handle, address, buffer, buffer.Length, out _)) {
                throw new MemoryAccessException("Cannot ReadProcessMemory: " + GetLastError().ToString());
            }

            return buffer;
        }

        public unsafe bool WriteMemory(IntPtr address, byte[] dataToWrite) {
            if (!WriteProcessMemory(this.handle, address, dataToWrite, dataToWrite.Length, out _)) {
                throw new MemoryAccessException("Cannot WriteProcessMemory: " + GetLastError().ToString());
            }
            return true;
        }

        public IntPtr FindDMAAddy(IntPtr ptr, int[] offsets) {
            return FindDMAAddy(this.handle, ptr, offsets);
        }

        public IntPtr GetModuleBaseAddress(string moduleName) {
            return GetModuleBaseAddress(this.process, moduleName);
        }
    }
}