# Call of Duty 4 External Radar
A C# external radar using WPF to render player/AI data structures read from game memory.

## Why?
* Game hacking is the best documented way to learn reverse engineering (my end goal)
* COD4 is old, fast to load in case of crashes/bugs
* It's single-player - it won't ruin other people's games
* It's unsecured and easier to hack

## Skills learned
* Finding addresses, pointers and structures in memory using CheatEngine
* Analysing assembly code using IDA Pro
* Loading raw-byte data into class instances
* Real time high performance asynchrinous rendering using C# WPF

## Preview
![alt text](Resources/Preview.png "Radar preview")
![alt text](Resources/Preview%202.png "Preview of damage taken and crouched stance")

## Features
- Scroll to zoom
- Click-drag to move window
- Fully scalable using vector shapes
- Swivels with player direction
- Different icon for foot soldiers (green), captains (blue), enemies (red) and dogs
- Shows player's stance and health
- Changes icon size of entities dependant on their relative height difference (bigger if above player, smaller if beneath)

## Developers
Throughout my code, I refer to entities as "animals". Originally, I used "humans", but I since discovered the entity list I found in memory includes:
* The player (not always the first item in the array)
* Enemies
* Friendlies
* Attack dogs


### COD4.rcnet
This file is a ReClass.NET project.
It can be loaded to reverse the data structures I've uncovered in COD4.
